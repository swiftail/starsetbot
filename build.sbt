name := "starsetbot"

version := "0.1"

scalaVersion := "2.12.0"

resolvers += Resolver.JCenterRepository
resolvers += Classpaths.typesafeReleases

val d4jVersion = "3.0.8"
val ScalatraVersion = "2.5.4"

libraryDependencies ++= Seq(
    "com.discord4j" % "discord4j-core" % d4jVersion,
    "com.discord4j" % "discord4j-command" % d4jVersion,
    "io.projectreactor" % "reactor-scala-extensions_2.12" % "0.4.6",
    "org.scalatra" %% "scalatra" % ScalatraVersion,
    "org.scalatra" %% "scalatra-scalatest" % ScalatraVersion % "test",
    "org.eclipse.jetty" % "jetty-webapp" % "9.2.19.v20160908" % "runtime",
    "javax.servlet" % "javax.servlet-api" % "3.1.0" % "provided",
    "edu.uci.ics" % "crawler4j" % "4.4.0",
    "ch.qos.logback" % "logback-classic" % "1.1.2" % "runtime",
    "net.sourceforge.htmlunit" % "htmlunit"% "2.36.0",
    "com.google.cloud" % "google-cloud-dialogflow" % "0.108.0-alpha"
)

enablePlugins(SbtTwirl)
enablePlugins(ScalatraPlugin)

