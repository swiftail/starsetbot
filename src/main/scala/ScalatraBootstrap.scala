import javax.servlet.ServletContext
import org.scalatra.LifeCycle
import reactor.util.Loggers
import ru.swiftail.starsetbot.discord.Main
import ru.swiftail.starsetbot.scalatra.controllers.{IndexController, StarsetController}

class ScalatraBootstrap extends LifeCycle {
    override def init(context: ServletContext): Unit = {

        context.mount(new IndexController, "/")
        context.mount(new StarsetController, "/starset/")

        Loggers.useSl4jLoggers()

        new Thread(() => {
            Main.main()
        }, "StarsetBot main thread").start()


        // val fetcher = new FetcherTimer
        // fetcher.start()

    }
}
