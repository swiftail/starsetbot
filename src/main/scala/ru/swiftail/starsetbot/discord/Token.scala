package ru.swiftail.starsetbot.discord

import scala.io.Source
import scala.util.Try

object Token {

    def get: Option[String] = {
        file().toOption.filter(_.nonEmpty)
    }

    private def file() = Try {
        val source = Source.fromFile("keys/discord_token")
        try {
            source.getLines().toSeq.head
        } finally source.close()
    }

    private def getenv() = Option(System.getenv("starsetbot_token"))
}
