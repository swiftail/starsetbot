package ru.swiftail.starsetbot.discord

import ru.swiftail.starsetbot.discord.commands.CommonConnection
import ru.swiftail.starsetbot.discord.module.Modules

object Main {

    def main(): Unit = {

        val maybeToken = Token.get

        val token = maybeToken match {
            case None => throw new IllegalStateException("No token preset")
            case Some(value) => value
        }

        Export.client = new ClientProvider().get(token)

        Export.events = Export.client.getEventDispatcher

        CommonConnection.setupCommands()

        Modules.initialize()

        Export.client.login().block()
    }
}
