package ru.swiftail.starsetbot.discord.module.dialogflow

import java.io.FileInputStream
import java.util.UUID

import com.google.api.gax.core.FixedCredentialsProvider
import com.google.auth.oauth2.GoogleCredentials
import com.google.cloud.dialogflow.v2._
import com.google.common.collect.Lists

class DialogflowService {

    private val sessionId: String = UUID.randomUUID().toString

    private val projectId: String = "small-talk-jpjkos"

    private val keyPath = "./keys/googlekey.json"

    private val credentials: GoogleCredentials =
        GoogleCredentials
            .fromStream(new FileInputStream(keyPath))
            .createScoped(Lists.newArrayList("https://www.googleapis.com/auth/cloud-platform"))

    private val sessionSettings: SessionsSettings =
        SessionsSettings
            .newBuilder()
            .setCredentialsProvider(FixedCredentialsProvider.create(credentials))
            .build()

    private val sessionsClient: SessionsClient = SessionsClient.create(sessionSettings)

    private def createSessionForUser(userId: Long): SessionName = SessionName.of(projectId, userId.toString)


    def detectIntentText(
                            text: String,
                            userId: Long,
                            languageCode: String = "en"
                        ): QueryResult = {

        val textInput =
            TextInput
                .newBuilder
                .setText(text)
                .setLanguageCode(languageCode)
                .build()

        val queryInput =
            QueryInput
                .newBuilder
                .setText(textInput)
                .build()

        val resp = sessionsClient.detectIntent(createSessionForUser(userId), queryInput)

        resp.getQueryResult
    }

}
