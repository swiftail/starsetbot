package ru.swiftail.starsetbot.discord.module.dialogflow;

import com.google.cloud.dialogflow.v2.QueryResult;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.MessageChannel;
import discord4j.core.object.entity.PrivateChannel;
import discord4j.core.object.entity.User;
import ru.swiftail.starsetbot.discord.Export;
import ru.swiftail.starsetbot.discord.module.Module;
import scala.Function1;
import scala.collection.Seq;
import scala.collection.Seq$;
import scala.concurrent.ExecutionContext;
import scala.concurrent.Future;

public class DialogflowModule extends Module {

    private DialogflowService df = new DialogflowService();

    private void react(MessageCreateEvent event) {
        Message message = event.getMessage();

        if (!message.getAuthor().isPresent()) {
            return;
        }

        User author = message.getAuthor().get();

        if (author.isBot()) {
            return;
        }

        if (!message.getContent().isPresent()) {
            return;
        }

        String content = message.getContent().get();

        if (!content.startsWith("s.") || content.substring(2).trim().isEmpty()) {
            return;
        }

        String query = content.substring(2).trim();

        message.getChannel().subscribe(channel -> {
            if (channel instanceof PrivateChannel) {
                return;
            }

            QueryResult result = df.detectIntentText(query, author.getId().asLong(), "en");

            channel.createEmbed((e) -> {
                e.addField(result.getFulfillmentText(), result.getIntent().getDisplayName(), false);
                e.setFooter("conf: " + result.getIntentDetectionConfidence() * 100 + "%", null);
            }).subscribe();
        });

    }

    @Override
    public void initialize() {
        Export.events().on(MessageCreateEvent.class)
                .subscribe(this::react);
    }
}