package ru.swiftail.starsetbot.discord.module.mention

import discord4j.core.`object`.entity.Message
import discord4j.core.event.domain.message.MessageCreateEvent
import reactor.core.publisher.Mono
import reactor.core.scala.publisher.ScalaConverters._
import ru.swiftail.starsetbot.discord.Export
import ru.swiftail.starsetbot.discord.module.Module

import scala.io.Source
import scala.util.Random

class MentionModule extends Module {

    type MessageProvider = Message => Mono[Message]

    private val random = new Random()

    private val messages: Seq[MessageProvider] =
        Source.fromResource("messages.txt").getLines.map(this.message).toSeq

    private val actions: Seq[MessageProvider] = messages

    override def initialize(): Unit = {
        Export
            .events
            .on(classOf[MessageCreateEvent])
            .asScala
            .map(_.getMessage)
            .filter(message => {
                message
                    .getContent
                    .filter(content => {
                        (content.contains("starset") && content.contains("bot")) ||
                            message.getUserMentionIds.contains(Export.client.getSelfId.get())
                    })
                    .isPresent
            })
            .flatMap(m => randomAction(m))
            .subscribe()
    }

    private def randomAction: MessageProvider = actions(random.nextInt(actions.size))

    private def message(content: String): MessageProvider = { m =>
        m.getChannel.flatMap(_.createMessage(content))
    }
}
