package ru.swiftail.starsetbot.discord.module

import ru.swiftail.starsetbot.discord.module.dialogflow.DialogflowModule
import ru.swiftail.starsetbot.discord.module.mention.MentionModule

object Modules {

    private val modules: Seq[Module] = Seq(
        new MentionModule,
        new DialogflowModule
    )

    def initialize(): Unit = {
        modules.foreach(_.initialize())
    }

}
