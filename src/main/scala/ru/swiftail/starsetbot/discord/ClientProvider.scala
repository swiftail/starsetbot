package ru.swiftail.starsetbot.discord

import discord4j.core.{DiscordClient, DiscordClientBuilder}

class ClientProvider {

    def get(token: String): DiscordClient =
        new DiscordClientBuilder(token).build()

}
