package ru.swiftail.starsetbot.discord

import discord4j.core.DiscordClient
import discord4j.core.event.EventDispatcher

object Export {
    var events: EventDispatcher = _
    var client: DiscordClient = _
}
