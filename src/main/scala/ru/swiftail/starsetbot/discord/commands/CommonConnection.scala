package ru.swiftail.starsetbot.discord.commands

import discord4j.command.util.AbstractCommandDispatcher
import discord4j.command.CommandBootstrapper
import discord4j.core.event.domain.message.MessageCreateEvent
import org.reactivestreams.Publisher
import reactor.core.publisher.Mono
import ru.swiftail.starsetbot.discord.Export

object CommonConnection {

    class CommonDispatcher(prefix: String) extends AbstractCommandDispatcher {

        override def getPrefixes(event: MessageCreateEvent): Publisher[String] = Mono.just(prefix)

    }

    def setupCommands(): Unit = {
        val dispatcher = new CommonDispatcher("s!")
        val cb = new CommandBootstrapper(dispatcher)
        cb.addProvider(new CommonProvider)
        cb.attach(Export.client).subscribe()
    }
}
