package ru.swiftail.starsetbot.discord.commands

import discord4j.command.Command
import discord4j.core.event.domain.message.MessageCreateEvent

trait CommonCommand extends Command[MessageCreateEvent]
