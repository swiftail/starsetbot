package ru.swiftail.starsetbot.discord.commands

import discord4j.core.event.domain.message.MessageCreateEvent
import reactor.core.publisher.Mono
import reactor.core.scala.publisher.ScalaConverters._

class PingCommand extends CommonCommand {

    override def execute(event: MessageCreateEvent, context: MessageCreateEvent): Mono[Void] = {

        event.getMessage.getChannel
            .asScala
            .map(c => c.createMessage("Pong!"))
            .block()
            .`then`()

    }

}
