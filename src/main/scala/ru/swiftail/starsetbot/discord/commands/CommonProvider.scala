package ru.swiftail.starsetbot.discord.commands

import discord4j.command.{CommandProvider, ProviderContext}
import discord4j.core.event.domain.message.MessageCreateEvent
import reactor.core.publisher.Flux
import reactor.core.scala.publisher._


class CommonProvider extends CommandProvider[MessageCreateEvent] {
    override def provide(
                            context: MessageCreateEvent,
                            commandName: String,
                            commandStartIndex: Int,
                            commandEndIndex: Int
                        ): Flux[ProviderContext[MessageCreateEvent]] = {

        SMono.just(commandName)
            .map(_.toLowerCase())
            .map {
                case "ping" =>
                    ProviderContext.of(new PingCommand)
                case _ => null
            }
            .flux()
            .asJava()

    }
}
