package ru.swiftail.starsetbot.core.browser

import com.gargoylesoftware.htmlunit.{BrowserVersion, WebClient}
import com.gargoylesoftware.htmlunit.html.{HtmlPage, HtmlScript}

import scala.collection.JavaConverters._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

trait BrowserSupport {

    protected def findScripts(url: String, root: String): Future[Iterator[String]] = Future {

        val client = new WebClient(BrowserVersion.CHROME)
        val page = client.getPage[HtmlPage](url)

        Thread.sleep(5 * 1000)

        val scripts = page
            .getElementsByName("script")
            .iterator()
            .asScala
            .map(_.asInstanceOf[HtmlScript])
            .map(_.getSrcAttribute)
            .map(_.toLowerCase())
            .filter(_.contains(root.toLowerCase()))

        scripts
    }

}
