package ru.swiftail.starsetbot.core.fetcher

import java.util.{Timer, TimerTask}

import ru.swiftail.starsetbot.core.processor._

class FetcherTimer {

    private val processors: Seq[AbstractProcessor] = Seq(
        classOf[AmirealornotCrawlerProcessor],
        classOf[StarsetonlineCrawlerProcessor],
        classOf[StarsetonlineIndexBrowserProcessor]
    ).map(_.newInstance())

    private def tick(): Unit = {
        println("Starting mnqn crawl")

        processors.foreach(_.tick())
    }

    def start(): Unit = {
        val timer = new Timer("Starset Fetcher Timer")
        timer.schedule(new TimerTask {
            override def run(): Unit = {
                tick()
            }
        }, 1000L * 5, 1000L * 60 * 6)
        println("Scheduling timer")
    }
}
