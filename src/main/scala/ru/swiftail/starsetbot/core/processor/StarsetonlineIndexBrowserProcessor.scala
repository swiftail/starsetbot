package ru.swiftail.starsetbot.core.processor

import scala.concurrent.ExecutionContext.Implicits.global

class StarsetonlineIndexBrowserProcessor extends AbstractBrowserProcessor {

    override def tick(): Unit = {
        val scripts = findScripts("https://starsetonline.com", "starsetonline.com")

        scripts.map(_.foreach(println))
    }

}
