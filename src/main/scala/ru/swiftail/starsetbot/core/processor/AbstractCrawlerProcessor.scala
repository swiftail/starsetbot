package ru.swiftail.starsetbot.core.processor

import edu.uci.ics.crawler4j.crawler.Page
import ru.swiftail.starsetbot.core.crawler.Crawlers

import scala.concurrent.Future

abstract class AbstractCrawlerProcessor(
                                           urls: Seq[String],
                                           name: String,
                                           root: String
                                       ) extends AbstractProcessor {
    protected def crawl(): Future[Seq[Page]] = {
        Crawlers.createAndLaunchCollectingHandler(urls, name, root)
    }
}
