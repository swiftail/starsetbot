package ru.swiftail.starsetbot.core.processor

import scala.concurrent.ExecutionContext.Implicits.global

class AmirealornotCrawlerProcessor extends AbstractCrawlerProcessor(
    Seq(
        "https://www.amirealornot.com",
        "https://www.amirealornot.com/admin.html",
        "https://www.amirealornot.com/agents.html"
    ),
    "amirealornot",
    "amirealornot"
) {
    override def tick(): Unit = {
        crawl().map { data =>
            data.foreach(d => println(s"${d.getWebURL.getURL}"))
        }
    }
}
