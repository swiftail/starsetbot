package ru.swiftail.starsetbot.core.processor

import ru.swiftail.starsetbot.core.browser.BrowserSupport

abstract class AbstractBrowserProcessor extends AbstractProcessor with BrowserSupport
