package ru.swiftail.starsetbot.core.processor

import scala.concurrent.ExecutionContext.Implicits.global

class StarsetonlineCrawlerProcessor extends AbstractCrawlerProcessor(
    Seq("https://starsetonline.com"),
    "starsetonline",
    "starsetonline.com"
) {

    override def tick(): Unit = {
        crawl().map { data =>
            data.foreach(d => println(s"${d.getWebURL.getURL}"))
        }
    }

}
