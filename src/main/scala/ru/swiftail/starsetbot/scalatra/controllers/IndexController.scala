package ru.swiftail.starsetbot.scalatra.controllers

import org.scalatra._

class IndexController extends ScalatraServlet {

    get("/") {
        views.html.index()
    }

}
